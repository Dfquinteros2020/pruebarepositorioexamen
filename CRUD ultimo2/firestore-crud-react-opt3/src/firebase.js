import firebase from 'firebase/app'  
import 'firebase/firestore'
  
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyCcQWf7sn0u5zo3glslDBFTcbIIzXCjADo",
    authDomain: "fb-crud-react-opt3.firebaseapp.com",
    databaseURL: "https://fb-crud-react-opt3.firebaseio.com",
    projectId: "fb-crud-react-opt3",
    storageBucket: "fb-crud-react-opt3.appspot.com",
    messagingSenderId: "912525760899",
    appId: "1:912525760899:web:ef9fc5d05bbc38f0c07eb6"
  };
  // Initialize Firebase

  const fb = firebase.initializeApp(firebaseConfig);
  export const db = fb.firestore();  