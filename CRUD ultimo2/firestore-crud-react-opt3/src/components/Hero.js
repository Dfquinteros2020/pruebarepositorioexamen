import React from "react";
import "./App.css";

import Links from "./Links";
import {ToastContainer} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Hero = ({handleLogout}) => {
  return (
    <section className="hero">
      <nav>
        <h2>Lista de estudiantes</h2>
        <button onClick={handleLogout}>Salir</button>
      </nav>
    <div className='container p-4'>
      <div className="row">
      <Links />
      </div>
      <ToastContainer />
    </div>
    </section>
  );
};

export default Hero;
