import React, { useEffect, useState} from "react";
import LinkForm from "./LinkForm";

import {db} from "../firebase";
import {toast} from "react-toastify";

const Links = () => {

const [links, setLinks] = useState([]);
const [currentId, setCurrentId] = useState('');

/* Funciones relacionadas con los datos Añadir o Editar en Firebase*/
const addOrEditLink = async (linkObject) => {
    try {
        if(currentId === '') {
            await db.collection('links').doc().set(linkObject);
            toast('Nuevo Dato agregado', {type: 'success'});
        } else {
            await db.collection('links').doc(currentId).update(linkObject);
            toast('Dato Actualizado Exitosamente', {type: 'info'});
            setCurrentId('');
        }
    } catch (error) {
      console.error(error);  
    }
};

/* Elimina datos de Firebase */
const onDeleteLink = async (id) => {
    if (window.confirm("Está seguro de que quiere eliminar estos datos?")){
        await db.collection('links').doc(id).delete();
        toast('Dato Eliminado Correctamente', {type: 'error', autoClose: 2000,})
        
    }
};

/* Obtiene datos de Firebase */
const getLinks = async () => {
    db.collection('links').onSnapshot((querySnapshot) => {
        const docs = [];
        querySnapshot.forEach(doc => {
        docs.push({...doc.data(), id:doc.id});
    });
    setLinks(docs);
 }); 
};

useEffect(() => {
    getLinks();
}, []);

    return (
    <div className="container">
        <div className="row">    
        <div className="col-lg-8">
            <div className="card shadow-lg p-3 mb-5 bg-white"> 
            {/* Muestra en pantalla el título del formulario */}
            <div className="card-header">Crud React con Cloud Firebase</div>
            {/* Para el ingreso de datos en el formulario */}
            <LinkForm {...{addOrEditLink, currentId, links}} />
            </div>
        </div>
        {/* Muestra en pantalla los datos de Firebase */}
        <div className="col-lg-8">
        <div className="card shadow-lg p-3 mb-5 bg-white">
            {links.map((link) => (
            <div className="card mb-1" key={link.id}>
                <div className="card-body">
                    <div className="d-flex justify-content-between">
                    {/* Muestra el nombre */}    
                    <h4>{link.nombres}</h4>
                    {/* Muestra el icono para eliminar datos */}
                    <div>
                    <i className="material-icons text-danger" onClick={() => onDeleteLink(link.id)}>delete</i>
                    {/* Muestra el icono para modificar datos */}
                    <i className="material-icons" onClick={() => setCurrentId(link.id)}>create</i>
                    </div>
                    </div>    
                    <p>{link.correo}</p>
                    <p>{link.nacimiento}</p>
                    <p>{link.telefono}</p>
                    <p>{link.edad}{' año/s'}</p>
                </div>
            </div>     
        ))}
        </div>
        </div>
    </div>
    </div>    
    );
};

export default Links;